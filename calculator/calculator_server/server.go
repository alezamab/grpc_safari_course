package main

import (
	"context"
	calculator "github.com/alezama/GrpcCourse/calculator/calculatorpb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"math"
	"net"
)

type server struct {
}

func (s server) SquareRoot(ctx context.Context, request *calculator.SquareRootRequest) (*calculator.SquareRootResponse, error) {
	number := request.GetNumber()
	if number < 0 {
		return nil, status.Errorf(codes.InvalidArgument, "Request contains a negative number")
	}
	resp := calculator.SquareRootResponse{SquareRoot: math.Sqrt(number)}
	return &resp, nil
}

func (s server) FindMaximumNumber(numberServer calculator.CalculatorService_FindMaximumNumberServer) error {
	max := int32(0)
	for {
		req, err := numberServer.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Printf("Error receiving the request %v\n", err)
			return err
		}
		if req.Number > max {
			max = req.Number
			err = numberServer.Send(&calculator.FindMaximumResponse{Maximum: max})
		}

		if err != nil {
			log.Printf("Error sending the request %v", err)
			return err
		}
	}
	return nil

}

func (s server) CalculateAverage(averageServer calculator.CalculatorService_CalculateAverageServer) error {

	reqNumb := make([]int64, 0)

	for {
		req, err := averageServer.Recv()
		if err == io.EOF {
			sum := int64(0)
			for _, numb := range reqNumb {
				sum = sum + numb
			}
			avg := float32(sum) / float32(len(reqNumb))
			resp := calculator.CalculateAverageResponse{Average: avg}
			return averageServer.SendAndClose(&resp)
		}
		if err != nil {
			log.Fatalf("Error receiving request %v\n", err)
		}

		reqNumb = append(reqNumb, req.Number)
	}
}

func (s server) PrimeNumbersDecompose(request *calculator.PrimeNumberRequest,
	decomposeServer calculator.CalculatorService_PrimeNumbersDecomposeServer) error {
	composeNumber := request.GetComposeNumber()
	c := make(chan int)
	go primes(int(composeNumber), c)
	for elm := range c {
		res := calculator.PrimeNumberResponse{
			PrimeNumber: int32(elm),
		}
		decomposeServer.Send(&res)
	}
	return nil

}

func (s server) Sum(ctx context.Context, request *calculator.SumRequest) (*calculator.SumResponse, error) {
	num1 := request.GetNum1()
	num2 := request.GetNum2()
	resp := calculator.SumResponse{Result: num1 + num2}
	return &resp, nil

}

func primes(number int, c chan int) {

	if number%2 == 0 {
		number = number / 2
		c <- 2
	}
	for i := 2; i < int(math.Sqrt(float64(number))); i = i + 2 {
		for number%i == 0 {
			c <- i
			number = number / i
		}
	}
	if number > 2 {
		c <- number
	}
	close(c)
}

func main() {

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	log.Println("Server starting in port 50051")

	if err != nil {
		log.Fatalln("Error listening to address 50051")
	}

	s := grpc.NewServer()

	calculator.RegisterCalculatorServiceServer(s, &server{})
	reflection.Register(s)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("Error serving a message %v \n", err)
	}

}
