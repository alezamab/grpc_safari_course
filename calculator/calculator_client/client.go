package main

import (
	calculator "github.com/alezama/GrpcCourse/calculator/calculatorpb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
	"io"
	"log"
)

func main() {

	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Error dialing to the server %v\n", err)
	}
	defer cc.Close()

	csc := calculator.NewCalculatorServiceClient(cc)
	//doUnary(csc)
	//doStream(csc)
	//calculateMaximum(csc)
	calculateSqrtRoot(csc)

}

func calculateSqrtRoot(client calculator.CalculatorServiceClient) {

	req := calculator.SquareRootRequest{Number: 145.25}

	resp, err := client.SquareRoot(context.Background(), &req)

	if err != nil {
		grpcErr, ok := status.FromError(err)
		if ok {
			log.Printf("Error received: %s", grpcErr.Message())
			log.Printf("Code received: %d", grpcErr.Code())
			return
		}
		log.Fatalf("Error receiving the response %v\n", err)

	}
	log.Printf("The square root of %.2f is %.2f ", req.GetNumber(), resp.GetSquareRoot())
}

func doUnary(client calculator.CalculatorServiceClient) {
	req := calculator.SumRequest{
		Num1: 255,
		Num2: 4152,
	}
	resp, err := client.Sum(context.Background(), &req)

	if err != nil {
		log.Fatalf("Error obtaining response %v\n", err)
	}
	log.Printf("Response comming from sum: %v \n", resp.Result)
}

func doStream(client calculator.CalculatorServiceClient) {
	req := calculator.PrimeNumberRequest{
		ComposeNumber: 4587,
	}
	stream, err := client.PrimeNumbersDecompose(context.Background(), &req)

	if err != nil {
		log.Fatalf("Error obtaining response %v\n", err)
	}
	for {
		msg, err := stream.Recv()

		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Error receiving response %v", err)
		}
		log.Printf("Message received %v", msg.GetPrimeNumber())
	}
}

func calculateAverage(client calculator.CalculatorServiceClient) {
	avgclient, err := client.CalculateAverage(context.Background())
	if err != nil {
		log.Fatalf("Error generating average request %v\n", err)
	}
	avgNumbers := []int64{145, 25, 625, 6896, 125}
	for _, num := range avgNumbers {
		err := avgclient.Send(&calculator.CalculateAverageRequest{Number: num})
		if err != nil {
			log.Fatalf("Error sending request %v", err)
		}

	}
	resp, err := avgclient.CloseAndRecv()
	if err != nil {
		log.Fatalf("Error receiving response %v\n", err)
	}
	log.Printf("The average of the numbers are: %f \n", resp.GetAverage())

}

func calculateMaximum(client calculator.CalculatorServiceClient) {
	maxReq, err := client.FindMaximumNumber(context.Background())
	waitc := make(chan interface{})
	if err != nil {
		log.Fatalf("Error creating the client %v ", err)
	}
	numStream := []int32{25, 1455, 14, 485, 125696, 2, 4}
	go func() {
		for _, num := range numStream {
			err := maxReq.Send(&calculator.FindMaximumRequest{Number: num})
			if err != nil {
				log.Fatalf("Error sending the request", err)
			}
		}
		err := maxReq.CloseSend()
		if err != nil {
			log.Fatalf("Error closing the request %v", err)
		}
	}()

	go func() {
		for {
			resp, err := maxReq.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("Error reading response %v", err)
			}
			log.Printf("Maximum number so far %v", resp.GetMaximum())
		}
		close(waitc)
	}()

	<-waitc
}
