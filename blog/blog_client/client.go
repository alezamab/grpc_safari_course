package main

import (
	"context"
	"fmt"
	"github.com/alezama/GrpcCourse/blog/blogpb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
	"log"
)

func main() {
	fmt.Println("Blog client")
	opts := grpc.WithInsecure()
	cc, err := grpc.Dial("localhost:50051", opts)

	if err != nil {
		log.Fatalf("could not connect %v", err)
	}

	defer cc.Close()

	c := blogpb.NewBlogServiceClient(cc)

	//createBlog(c)
	//findBlog(c)
	updateBlog(c)

}

func findBlog(client blogpb.BlogServiceClient) {
	log.Println("Reading a blog")
	reponse, err := client.ReadBlog(context.Background(), &blogpb.ReadBlogRequest{BlogId: "5f1cfc2f1993aa1cf2114619"})
	if err != nil {
		rpcError, ok := status.FromError(err)
		if ok {
			log.Printf("Error in the response %d, %v", rpcError.Code(), rpcError.Message())
			return
		}
		log.Printf("Unknow error receiving the response %v\n", err)
	}
	log.Printf("Result blog: %v\n", reponse.Blog)
}

func updateBlog(client blogpb.BlogServiceClient) {
	log.Println("Updating a blog")
	response, err := client.UpdateBlog(context.Background(), &blogpb.UpdateBlogRequest{Blog: &blogpb.Blog{
		Id:       "5f1cfc2f1993aa1cf2114619",
		AuthorId: "2",
		Title:    "Los riesgos de militarizar los puertos",
		Content:  "El contenido es restringido",
	}})
	if err != nil {
		log.Printf("Error updating the blog %v", err)
		return
	}
	if response.Updated != 0 {
		log.Printf("The blog has been updated ")
		return
	}

}

func createBlog(client blogpb.BlogServiceClient) {
	blog := &blogpb.Blog{
		AuthorId: "1",
		Title:    "La nueva normalidad",
		Content:  "La nueva normalidad está originando que muchos negocios se pierdan",
	}
	req := blogpb.CreateBlogRequest{Blog: blog}
	resp, err := client.CreateBlog(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok {
			log.Printf("RPC error from request %d: %v", st.Err(), st.Message())
			return
		}
		log.Fatalf("No a RPC error %v", err)
	}
	log.Printf("The blog has been created with the ID: %s\n", resp.Blog.Id)
}
