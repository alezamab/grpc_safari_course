package main

import (
	"context"
	"fmt"
	"github.com/alezama/GrpcCourse/blog/blogpb"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"log"
	"net"
	"os"
	"os/signal"
	"time"
)

type server struct {
}

func (s server) UpdateBlog(ctx context.Context, request *blogpb.UpdateBlogRequest) (*blogpb.UpdateBlogResponse, error) {

	if request.GetBlog() == nil {
		return nil, status.Errorf(codes.InvalidArgument, "The blog in the request is nil")
	}

	id := request.GetBlog().GetId()
	data, err := findBlogById(id)
	if err != nil {
		return nil, err
	}
	if request.GetBlog().GetAuthorId() != "" {
		data.AuthorID = request.GetBlog().GetAuthorId()
	}
	if request.GetBlog().GetTitle() != "" {
		data.Title = request.GetBlog().GetTitle()
	}
	if request.GetBlog().GetContent() != "" {
		data.Content = request.GetBlog().GetContent()
	}
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Cannot parse id %v", err))
	}
	filter := bson.D{{"_id", oid}}
	resp, err := collection.UpdateOne(context.Background(), filter, data)
	if err != nil {
		return nil, status.Errorf(codes.Internal, fmt.Sprintf("The blog could not be updated %v", err))
	}

	return &blogpb.UpdateBlogResponse{Updated: resp.ModifiedCount}, nil
}

func (s server) ReadBlog(ctx context.Context, request *blogpb.ReadBlogRequest) (*blogpb.ReadBlogResponse, error) {
	id := request.GetBlogId()

	data, err := findBlogById(id)
	if err != nil {
		return nil, err
	}

	return &blogpb.ReadBlogResponse{Blog: &blogpb.Blog{
		Id:       primitive.ObjectID.Hex(data.ID),
		AuthorId: data.AuthorID,
		Title:    data.Title,
		Content:  data.Content,
	}}, nil
}

func findBlogById(id string) (*blogItem, error) {
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Cannot parse id %v", err))
	}

	data := &blogItem{}
	filter := bson.D{{"_id", oid}}
	res := collection.FindOne(context.Background(), filter)
	if err := res.Decode(data); err != nil {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("Cannot found record %v", err))
	}

	return data, nil
}

func (s server) CreateBlog(ctx context.Context, request *blogpb.CreateBlogRequest) (*blogpb.CreateBlogResponse, error) {
	blog := request.Blog
	data := blogItem{
		AuthorID: blog.AuthorId,
		Content:  blog.Content,
		Title:    blog.Title,
	}
	res, err := collection.InsertOne(context.Background(), data)
	if err != nil {
		log.Printf("Returning the error code")
		return nil, status.Errorf(codes.Internal, fmt.Sprintf("Internal error %v", err))
	}
	oid, ok := res.InsertedID.(primitive.ObjectID)
	if !ok {
		log.Printf("Error parsing the objetid")
		return nil, status.Errorf(codes.Internal, fmt.Sprintf("Error retrieving the object id"))
	}
	returnBlog := blogpb.Blog{
		Id:       oid.Hex(),
		AuthorId: data.AuthorID,
		Title:    data.Title,
		Content:  data.Content,
	}
	return &blogpb.CreateBlogResponse{Blog: &returnBlog}, nil
}

type blogItem struct {
	ID       primitive.ObjectID `bson:"_id,omitempty"`
	AuthorID string             `bson:"author_id"`
	Content  string             `bson:"content"`
	Title    string             `bson:"title"`
}

var collection *mongo.Collection

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Println("Blog service started")

	mgdClient, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatalf("Error opening mongoDB connection %v", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	err = mgdClient.Connect(ctx)
	if err != nil {
		log.Fatalf("Error opening mongoDB connection %v", err)
	}

	collection = mgdClient.Database("mydb").Collection("blog")

	list, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen port %v", err)
	}
	opts := []grpc.ServerOption{}
	s := grpc.NewServer(opts...)
	blogpb.RegisterBlogServiceServer(s, &server{})

	go func() {
		if err := s.Serve(list); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	<-ch
	log.Println("Stopping the server")
	s.Stop()
	log.Println("Closing the listener")
	list.Close()
	log.Println("END Program")
}
