package main

import (
	"context"
	"github.com/alezama/GrpcCourse/greet/greetpb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"net"
	"strconv"
	"time"
)

type server struct {
}

func (s *server) GreetWithDeadLine(ctx context.Context, request *greetpb.GreetWithDeadLineRequest) (*greetpb.GreetWithDeadLineResponse, error) {
	firstName := request.GetGreeting().GetFirstName()
	ticker := time.NewTicker(500 * time.Microsecond)
	done := make(chan bool)
	go func() {
		for {
			select {
			case <-ticker.C:
				if ctx.Err() == context.DeadlineExceeded {
					done <- false
				}
			}
		}
	}()

	go func() {
		time.Sleep(2 * time.Second)
		done <- true
	}()

	chResult := <-done

	if chResult {
		result := "Hello " + firstName
		res := greetpb.GreetWithDeadLineResponse{Response: result}
		return &res, nil
	}

	log.Println("The client timeout a request")
	return nil, status.Errorf(codes.Canceled, "Request canceled by the client")
}

func (s *server) GreetEveryone(everyoneServer greetpb.GeetService_GreetEveryoneServer) error {
	for {
		request, err := everyoneServer.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Error receiving request %v", err)
			return err
		}
		firstName := request.GetGreeting().GetFirstName()
		err = everyoneServer.Send(&greetpb.GreetEveryoneResponse{Response: "Hi " + firstName})
		if err != nil {
			log.Fatalf("Error receiving request %v", err)
			return err
		}
	}
	return nil
}

func (s *server) Geet(ctx context.Context, request *greetpb.GreetRequest) (*greetpb.GreetResponse, error) {
	firstName := request.GetGreeting().GetFirstName()
	result := "Hello " + firstName
	res := greetpb.GreetResponse{Response: result}
	return &res, nil
}

func (s *server) GreetManyTimes(request *greetpb.GreetManyTimesRequest,
	stream greetpb.GeetService_GreetManyTimesServer) error {
	firstName := request.Greeting.FirstName
	for i := 0; i < 10; i++ {
		response := "Hello " + firstName + " number " + strconv.Itoa(i)
		res := greetpb.GreetManyTimesResponse{Response: response}
		stream.Send(&res)
		time.Sleep(1 * time.Second)
	}
	return nil
}

func (s *server) LongGreet(stream greetpb.GeetService_LongGreetServer) error {
	var reqSlice = make([]string, 0)
	for {
		longRequest, err := stream.Recv()
		if err == io.EOF {
			//we have finish reading the client request
			buffStr := "Hi: "
			for _, pieceStr := range reqSlice {
				buffStr = buffStr + "\n" + pieceStr
			}

			response := greetpb.LongGreetResponse{
				Response: buffStr,
			}
			//  the send and close returns and error so it can be used to finish the function
			return stream.SendAndClose(&response)

		}
		if err != nil {
			log.Fatalf("Error receiving request stream %v \n", err)
		}
		reqSlice = append(reqSlice, longRequest.GetGreeting().FirstName)
	}
}

func main() {
	log.Println("Server starting in port 500051")
	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v\n", err)
	}

	s := grpc.NewServer()
	greetpb.RegisterGeetServiceServer(s, &server{})

	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve %v\n", err)
	}

}
