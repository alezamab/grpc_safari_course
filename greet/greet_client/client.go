package main

import (
	"context"
	"github.com/alezama/GrpcCourse/greet/greetpb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"time"
)

func main() {
	log.Println("Client starting ...")
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalln("Could not connect %v", err)
	}
	defer conn.Close()
	c := greetpb.NewGeetServiceClient(conn)
	//doUnary(c)
	//doStream(c)
	//doClientStreaming(c)
	//doBiDiClient(c)
	greetingWithDeadline(c, 1*time.Second)
	greetingWithDeadline(c, 3*time.Second)

}

func doUnary(c greetpb.GeetServiceClient) {
	req := &greetpb.GreetRequest{
		Greeting: &greetpb.Greeting{
			FirstName: "Anuar",
			LastName:  "Lezama",
		},
	}
	greetResponse, error := c.Geet(context.Background(), req)
	if error != nil {
		log.Fatalf("Error while calling Greet rpc: %v", error)
	}
	log.Printf("Response Greeting: \"%s\"", greetResponse.Response)
}

func greetingWithDeadline(c greetpb.GeetServiceClient, t time.Duration) {
	req := &greetpb.GreetWithDeadLineRequest{
		Greeting: &greetpb.Greeting{
			FirstName: "Anuar",
			LastName:  "Lezama",
		},
	}
	ctx, cancel := context.WithTimeout(context.Background(), t)
	defer cancel()
	greetResponse, err := c.GreetWithDeadLine(ctx, req)
	if err != nil {
		grpcErr, ok := status.FromError(err)
		if ok {
			if grpcErr.Code() == codes.DeadlineExceeded {
				log.Println("Deadline for the request excedeed")
				return
			}
			log.Printf("Unexpected grpc error: %s \n", grpcErr.Message())
			return
		}
		log.Fatalf("Error while calling GreetWithDeadline rpc: %v", err)
	}
	log.Printf("Response GreetingWithDeadline: \"%s\"", greetResponse.Response)
}

func doStream(c greetpb.GeetServiceClient) {
	req := &greetpb.GreetManyTimesRequest{
		Greeting: &greetpb.Greeting{
			FirstName: "Anuar",
			LastName:  "Lezama",
		},
	}
	stream, error := c.GreetManyTimes(context.Background(), req)
	if error != nil {
		log.Fatalf("Error while calling GreetManyTimes %v", error)
	}
	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Error while receiving messages from stream %v", err)
		}
		log.Printf("Message received: %s", msg.GetResponse())
	}

}

func doClientStreaming(c greetpb.GeetServiceClient) {
	namesSeq := []string{"Juan", "James", "Domingo", "Carlos", "Edgar"}
	client, err := c.LongGreet(context.Background())
	if err != nil {
		log.Fatalf("Error creating client context %v", err)
	}
	for _, name := range namesSeq {
		greeting := greetpb.Greeting{
			FirstName: name,
			LastName:  "anything",
		}

		longGreetReq := greetpb.LongGreetRequest{Greeting: &greeting}
		err := client.Send(&longGreetReq)
		if err != nil {
			log.Fatalf("Error sending request %v", err)
		}
	}

	resp, err := client.CloseAndRecv()
	if err != nil {
		log.Fatalf("Error closing the client %v", err)
	}
	log.Println(resp.GetResponse())
}

func doBiDiClient(c greetpb.GeetServiceClient) {
	stream, err := c.GreetEveryone(context.Background())
	if err != nil {
		log.Fatalf("Error while creating stream: %v\n", err)
		return
	}
	waitc := make(chan struct{})
	go func() {
		namesSeq := []string{"Juan", "James", "Domingo", "Carlos", "Edgar"}
		for _, name := range namesSeq {
			err := stream.Send(&greetpb.GreetEveryoneRequest{
				Greeting: &greetpb.Greeting{
					FirstName: name,
					LastName:  "anything",
				},
			})
			if err != nil {
				log.Fatalf("Error sending the request %v\n", err)
			}
		}
		err := stream.CloseSend()
		if err != nil {
			log.Fatalf("Error closing the stream %v", err)
		}
	}()

	go func() {
		for {
			resp, err := stream.Recv()
			if err == io.EOF {
				close(waitc)
				break
			}
			if err != nil {
				log.Fatalf("Error reading the response %v", err)
			}
			log.Printf("Receiving the response %v", resp.GetResponse())
		}
	}()
	<-waitc
}
